//: This is the playground file to use for submitting HW1.  You will add your code where noted below.  Make sure you only put the code required at load time in the ``loadView()`` method.  Other code should be set up as additional methods (such as the code called when a button is pressed).
  
import UIKit
import PlaygroundSupport
import Foundation

enum Gender : String {
    case Male = "Male"
    case Female = "Female"
    case NonBinary = "Non-binary"
}

class Person {
    var firstName = ""
    var lastName = ""
    var whereFrom = ""  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
    var hobbies : [String] = []
}

enum DukeRole : String {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
    case Other = "Other"
}

protocol ECE564 {
    var degree : String { get }
    var languages: [String] { get }
    var team : String { get }
}

// custom protocol
protocol CustomStringConvertible {
    var description: String { get }
}

// new sub class
class DukePerson : Person, ECE564, CustomStringConvertible {
    var netID : String = ""
    var degree : String = ""
    var languages : [String] = []
    var team : String = ""
    var role : DukeRole = .Professor
    var description: String{
        get{
            return "This is \(firstName) \(lastName) from \(whereFrom)! \(self.gender == .Male ? "He" : "She") is a \(self.role == .Professor ? "professor" : self.role == .TA ? "TA" : "student") at Duke University"
        }
    }
    override init () {
        super.init()
    }
    init (netID: String, firstName: String, lastName: String, whereFrom: String, gender: Gender, hobbies: [String], degree: String, languages: [String], team: String, role: DukeRole) {
        super.init()
        self.netID = netID
        self.firstName = firstName
        self.lastName = lastName
        self.whereFrom = whereFrom
        self.gender = gender
        self.hobbies = hobbies
        self.degree = degree
        self.team = team
        self.languages = languages
        self.role = role
    }
    func getCopy() -> DukePerson {
        let newInstance = DukePerson()
        newInstance.netID = self.netID
        newInstance.firstName = self.firstName
        newInstance.lastName = self.lastName
        newInstance.whereFrom = self.whereFrom
        newInstance.gender = self.gender
        newInstance.hobbies = []
        for idx in self.hobbies.indices {
            newInstance.hobbies.append(self.hobbies[idx])
        }
        newInstance.degree = self.degree
        newInstance.team = self.team
        newInstance.role = self.role
        newInstance.languages = []
        for idx in self.languages.indices {
            newInstance.languages.append(self.languages[idx])
        }
        
        return newInstance
    }
}

class Store {
    var members : [DukePerson] = [];
    func addMember(newMember: DukePerson) -> Store {
        members.append(newMember)
        return self
    }
    func updateMember(index: Int, updatedMember: DukePerson) -> Bool {
        members[index] = updatedMember
        return true;
    }
    func getMembers() -> [DukePerson] {
        return members;
    }
}


func initData() -> Store {
    let store = Store()
    store
        .addMember(newMember: DukePerson(netID: "zl248", firstName: "Zhu", lastName: "Li", whereFrom: "China", gender: Gender.Male, hobbies: ["reading", "tennis", "hiking"], degree: "ECE", languages: ["Javasript", "C++", "Java"], team: "0", role: .Student))
        .addMember(newMember: DukePerson(netID: "rt113", firstName: "Richard", lastName: "Telford", whereFrom: "Chatham County, NC", gender: Gender.Male, hobbies: ["Swimming", "Biking", "Reading"], degree: "", languages: ["Swift", "C", "C++"], team: "0", role: .Professor))
        .addMember(newMember: DukePerson(netID: "as866", firstName: "Abhijay", lastName: "Suhag", whereFrom: "Augusta, Georgia", gender: Gender.Male, hobbies: ["Hiking", "Gaming", "Playing tennis"], degree: "", languages: ["Typescript", "Swift", "Java"], team: "0", role: .TA))
        .addMember(newMember: DukePerson(netID: "ak513", firstName: "Andrew", lastName: "Krier", whereFrom: "Saint Paul, Minnesota", gender: Gender.Male, hobbies: ["Basketball", "Frisbee"], degree: "", languages: ["Swift", "Java", "Python"], team: "0", role: .TA))
    return store
}


// data logic configuration
let _store = initData()
var page_num = 0
var members = _store.getMembers()
var total = members.count
var curr_total = total
var curr_copy = _store.getMembers()[page_num].getCopy()
var searched_idx: [Int] = []
for i in 0..<total{
    searched_idx.append(i)
}
let blankTemplate = DukePerson()
var blankMode = false


// UI configuration
let titleLabel = UILabel()
let searchField = UITextField()
let clearButton = UIButton()
let clearImg = UIImage(named: "ClearOutline.png")
let searchButton = UIButton()
let searchImg = UIImage(named: "find.png")
let firstNameLabel = UILabel()
let firstNameTextField = UITextField()
let lastNameLabel = UILabel()
let lastNameTextField = UITextField()
let netIDLabel = UILabel()
let netIDTextField = UITextField()
let fromLabel = UILabel()
let fromTextField = UITextField()
let genderLabel = UILabel()
let genderSeg = UISegmentedControl.init()
let hobbiesLabel = UILabel()
let hobbiesField = UITextField()
let languagesLabel = UILabel()
let languagesField = UITextField()
let roleLabel = UILabel()
let roleSeg = UISegmentedControl.init()
let descriptionLabel = UILabel()
let imageView = UIImageView(image: UIImage(named: "duke.jpg"))
let addButton = UIButton()
let updateButton = UIButton()
let lastButton = UIButton()
let nextButton = UIButton()
let pageLabel = UILabel()

titleLabel.frame = CGRect(x: 100, y: 10, width: 200, height: 20)
titleLabel.text = "ECE 564 Homework 1"
titleLabel.textColor = .black
titleLabel.font = UIFont(name: "Noteworthy", size: 20)

searchField.frame = CGRect(x: 50, y: 60, width: 145, height: 30)
searchField.borderStyle = UITextField.BorderStyle.roundedRect
searchField.autocapitalizationType = .none
searchField.placeholder = "Search by netID"
searchField.font = UIFont(name: "Cochin", size: 17)
searchField.inputView = UIView()

clearButton.frame = CGRect(x: 210, y: 57, width: 60, height: 36)
clearButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
clearButton.setImage(clearImg, for: UIControl.State())
clearButton.layer.cornerRadius = 6
clearButton.isHidden = false
clearButton.titleLabel?.numberOfLines = 0
clearButton.titleLabel?.textAlignment = .center
clearButton.titleLabel?.lineBreakMode = .byWordWrapping

searchButton.frame = CGRect(x: 285, y: 57, width: 60, height: 36)
searchButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
searchButton.setImage(searchImg, for: UIControl.State())
searchButton.layer.cornerRadius = 6
searchButton.isHidden = false
searchButton.titleLabel?.numberOfLines = 0
searchButton.titleLabel?.textAlignment = .center
searchButton.titleLabel?.lineBreakMode = .byWordWrapping

netIDLabel.frame = CGRect(x: 50, y: 120, width: 100, height: 20)
netIDLabel.text = "Net ID:"
netIDLabel.textColor = .black
netIDLabel.font = UIFont(name: "Noteworthy", size: 18)

netIDTextField.frame = CGRect(x: 170, y: 115, width: 175, height: 30)
netIDTextField.borderStyle = UITextField.BorderStyle.roundedRect
netIDTextField.textColor = .black
netIDTextField.placeholder = "e.g. zl248"
netIDTextField.font = UIFont(name: "Cochin", size: 17)
netIDTextField.inputView = UIView()

firstNameLabel.frame = CGRect(x: 50, y: 160, width: 100, height: 20)
firstNameLabel.text = "First Name:"
firstNameLabel.textColor = .black
firstNameLabel.font = UIFont(name: "Noteworthy", size: 18)

firstNameTextField.frame = CGRect(x: 170, y: 155, width: 175, height: 30)
firstNameTextField.borderStyle = UITextField.BorderStyle.roundedRect
firstNameTextField.textColor = .black
firstNameTextField.placeholder = "e.g. Zhu"
firstNameTextField.font = UIFont(name: "Cochin", size: 17)
firstNameTextField.inputView = UIView()

lastNameLabel.frame = CGRect(x: 50, y: 200, width: 100, height: 20)
lastNameLabel.text = "Last Name:"
lastNameLabel.textColor = .black
lastNameLabel.font = UIFont(name: "Noteworthy", size: 18)

lastNameTextField.frame = CGRect(x: 170, y: 195, width: 175, height: 30)
lastNameTextField.borderStyle = UITextField.BorderStyle.roundedRect
lastNameTextField.textColor = .black
lastNameTextField.placeholder = "e.g. Li"
lastNameTextField.font = UIFont(name: "Cochin", size: 17)
lastNameTextField.inputView = UIView()

fromLabel.frame = CGRect(x: 50, y: 240, width: 100, height: 20)
fromLabel.text = "From:"
fromLabel.textColor = .black
fromLabel.font = UIFont(name: "Noteworthy", size: 18)

fromTextField.frame = CGRect(x: 170, y: 235, width: 175, height: 30)
fromTextField.borderStyle = UITextField.BorderStyle.roundedRect
fromTextField.textColor = .black
fromTextField.placeholder = "e.g. China"
fromTextField.font = UIFont(name: "Cochin", size: 17)
fromTextField.inputView = UIView()

genderLabel.frame = CGRect(x: 50, y: 280, width: 100, height: 20)
genderLabel.text = "Gender:"
genderLabel.textColor = .black
genderLabel.font = UIFont(name: "Noteworthy", size: 18)

genderSeg.insertSegment(withTitle: "Male", at: 0, animated: false)
genderSeg.insertSegment(withTitle: "Female", at: 1, animated: false)
genderSeg.frame = CGRect(x: 170, y: 275, width: 175, height: 30)
genderSeg.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.5))
genderSeg.tintColor = .black

hobbiesLabel.frame = CGRect(x: 50, y: 320, width: 100, height: 20)
hobbiesLabel.text = "Hobbies:"
hobbiesLabel.textColor = .black
hobbiesLabel.font = UIFont(name: "Noteworthy", size: 18)

hobbiesField.frame = CGRect(x: 170, y: 315, width: 175, height: 30)
hobbiesField.borderStyle = UITextField.BorderStyle.roundedRect
hobbiesField.textColor = .black
hobbiesField.placeholder = "e.g. Hiking, Tennis"
hobbiesField.font = UIFont(name: "Cochin", size: 17)
hobbiesField.inputView = UIView()

languagesLabel.frame = CGRect(x: 50, y: 360, width: 100, height: 20)
languagesLabel.text = "Languages:"
languagesLabel.textColor = .black
languagesLabel.font = UIFont(name: "Noteworthy", size: 18)

languagesField.frame = CGRect(x: 170, y: 355, width: 175, height: 30)
languagesField.borderStyle = UITextField.BorderStyle.roundedRect
languagesField.textColor = .black
languagesField.placeholder = "e.g. Javascript, C++"
languagesField.font = UIFont(name: "Cochin", size: 17)
languagesField.inputView = UIView()

roleLabel.frame = CGRect(x: 50, y: 400, width: 100, height: 20)
roleLabel.text = "Role:"
roleLabel.textColor = .black
roleLabel.font = UIFont(name: "Noteworthy", size: 18)

roleSeg.insertSegment(withTitle: "Prof", at: 0, animated: false)
roleSeg.insertSegment(withTitle: "TA", at: 1, animated: false)
roleSeg.insertSegment(withTitle: "Student", at: 2, animated: false)
roleSeg.frame = CGRect(x: 170, y: 395, width: 175, height: 30)
roleSeg.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.5))
roleSeg.tintColor = .black

descriptionLabel.frame = CGRect(x: 50, y: 440, width: 295, height: 80)
descriptionLabel.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.586, green: 0.87, blue: 0.95, alpha: 1))
descriptionLabel.lineBreakMode = .byWordWrapping
descriptionLabel.numberOfLines = 0
descriptionLabel.lineBreakStrategy = .standard
descriptionLabel.textAlignment = .center
descriptionLabel.textColor = .white
descriptionLabel.font = UIFont(name: "Noteworthy", size: 18)

imageView.frame = CGRect(x: 60, y: 140, width: 250, height: 250)
imageView.alpha = 0.06

addButton.frame = CGRect(x: 50, y: 550, width:75, height: 38)
//addButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
addButton.backgroundColor = .lightGray
addButton.layer.cornerRadius = 8
addButton.isHidden = false
addButton.titleLabel?.numberOfLines = 0
addButton.titleLabel?.textAlignment = .center
addButton.titleLabel?.lineBreakMode = .byWordWrapping
addButton.setTitle("Add", for: UIControl.State())
addButton.titleLabel?.font = UIFont(name: "Noteworthy", size: CGFloat(18))
addButton.setTitleColor(UIColor.white, for: UIControl.State())
addButton.setTitleColor(UIColor.red, for: .highlighted)

updateButton.frame = CGRect(x: 142, y: 550, width:75, height: 38)
updateButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
updateButton.layer.cornerRadius = 8
updateButton.isHidden = false
updateButton.titleLabel?.numberOfLines = 0
updateButton.titleLabel?.textAlignment = .center
updateButton.titleLabel?.lineBreakMode = .byWordWrapping
updateButton.setTitle("Update", for: UIControl.State())
updateButton.titleLabel?.font = UIFont(name: "Noteworthy", size: CGFloat(18))
updateButton.setTitleColor(UIColor.white, for: UIControl.State())
updateButton.setTitleColor(UIColor.red, for: .highlighted)

lastButton.frame = CGRect(x: 263, y: 550, width:32, height: 38)
lastButton.layer.cornerRadius = 16
lastButton.isHidden = false
lastButton.titleLabel?.numberOfLines = 0
lastButton.titleLabel?.textAlignment = .center
lastButton.titleLabel?.lineBreakMode = .byWordWrapping
lastButton.setTitle("<", for: UIControl.State())
lastButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(18))
lastButton.setTitleColor(UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8)), for: UIControl.State())
lastButton.setTitleColor(UIColor.red, for: .highlighted)

nextButton.frame = CGRect(x: 315, y: 550, width:32, height: 38)
nextButton.layer.cornerRadius = 16
nextButton.isHidden = false
nextButton.titleLabel?.numberOfLines = 0
nextButton.titleLabel?.textAlignment = .center
nextButton.titleLabel?.lineBreakMode = .byWordWrapping
nextButton.setTitle(">", for: UIControl.State())
nextButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: CGFloat(18))
nextButton.setTitleColor(UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8)), for: UIControl.State())
nextButton.setTitleColor(UIColor.red, for: .highlighted)

pageLabel.frame = CGRect(x: 291, y: 550, width: 50, height: 38)
pageLabel.font = UIFont(name: "Noteworthy", size: CGFloat(14))
pageLabel.textColor = .darkGray

// main controller
class HW1ViewController : UIViewController {
//    override func viewDidLoad() {
//        super.viewDidLoad()
//        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
//        view.addGestureRecognizer(tap)
//    }
//
//    @objc func dismissKeyboard() {
//        view.endEditing(true)
//    }
    
    @objc func fire() {
        descriptionLabel.text = curr_copy.description
    }
    
    @objc func fireFail() {
        addButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
        updateButton.backgroundColor = .lightGray
        descriptionLabel.text = "Add new one's Info and ues Add button below to upload"
    }
    
    override func loadView() {
        let view = UIView()
        loadAction()
        renderUpdate()
        view.backgroundColor = .white
        view.addSubview(titleLabel)
        view.addSubview(searchField)
        view.addSubview(searchButton)
        view.addSubview(clearButton)
        view.addSubview(netIDLabel)
        view.addSubview(netIDTextField)
        view.addSubview(firstNameLabel)
        view.addSubview(firstNameTextField)
        view.addSubview(lastNameLabel)
        view.addSubview(lastNameTextField)
        view.addSubview(fromLabel)
        view.addSubview(fromTextField)
        view.addSubview(genderLabel)
        view.addSubview(genderSeg)
        view.addSubview(hobbiesLabel)
        view.addSubview(hobbiesField)
        view.addSubview(languagesLabel)
        view.addSubview(languagesField)
        view.addSubview(roleLabel)
        view.addSubview(roleSeg)
        view.addSubview(descriptionLabel)
        view.addSubview(imageView)
        view.addSubview(addButton)
        view.addSubview(updateButton)
        view.addSubview(lastButton)
        view.addSubview(nextButton)
        view.addSubview(pageLabel)
        self.view = view
    }
    
    func renderUpdate() {
        print("render update")
        updateButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
        addButton.backgroundColor = .lightGray
        blankMode = false
        total = members.count
        curr_copy = members[searched_idx[page_num]].getCopy()
        curr_total = searched_idx.count
        netIDTextField.text = curr_copy.netID
        firstNameTextField.text = curr_copy.firstName
        lastNameTextField.text = curr_copy.lastName
        fromTextField.text = curr_copy.whereFrom
        genderSeg.selectedSegmentIndex = curr_copy.gender == .Male ? 0: 1
        hobbiesField.text = curr_copy.hobbies.joined(separator: ",")
        languagesField.text = curr_copy.languages.joined(separator: ",")
        roleSeg.selectedSegmentIndex = curr_copy.role == .Professor ? 0 : curr_copy.role == .TA ? 1 : 2
        descriptionLabel.text = curr_copy.description
        pageLabel.text = String(page_num+1) + " / " + String(curr_total)
    }
    
    func renderBlank() {
        print("render blank")
        addButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
        updateButton.backgroundColor = .lightGray
        blankMode = true
        curr_copy = blankTemplate.getCopy();
        page_num = 0
        curr_total = 1
        netIDTextField.text = curr_copy.netID
        firstNameTextField.text = curr_copy.firstName
        lastNameTextField.text = curr_copy.lastName
        fromTextField.text = curr_copy.whereFrom
        genderSeg.selectedSegmentIndex = curr_copy.gender == .Male ? 0: 1
        hobbiesField.text = curr_copy.hobbies.joined(separator: ",")
        languagesField.text = curr_copy.languages.joined(separator: ",")
        roleSeg.selectedSegmentIndex = curr_copy.role == .Professor ? 0 : curr_copy.role == .TA ? 1 : 2
        descriptionLabel.text = curr_copy.description
        pageLabel.text = String(page_num+1) + " / " + String(curr_total)
    }
    
    func loadAction() {// UI event
        netIDTextField.addAction(UIAction { _ in
            print("net id changed")
            curr_copy.netID = netIDTextField.text ?? ""
        }, for: .editingChanged)
        
        firstNameTextField.addAction(UIAction { _ in
            print("name changed")
            curr_copy.firstName = firstNameTextField.text ?? ""
        }, for: .editingChanged)

        lastNameTextField.addAction(UIAction { _ in
            print("name changed")
            curr_copy.lastName = lastNameTextField.text ?? ""
        }, for: .editingChanged)

        fromTextField.addAction(UIAction { _ in
            print("from changed")
            curr_copy.whereFrom = fromTextField.text ?? ""
        }, for: .editingChanged)

        genderSeg.addAction(UIAction { _ in
            print("gender changed")
            if (genderSeg.selectedSegmentIndex == 0) {
                curr_copy.gender = .Male
            } else {
                curr_copy.gender = .Female
            }
        }, for: .valueChanged)

        hobbiesField.addAction(UIAction { _ in
            print("hobbies changed")
            curr_copy.hobbies = "\(hobbiesField.text!)".split(separator: ",").map { String($0)}
        }, for: .editingChanged)

        languagesField.addAction(UIAction { _ in
            print("languages changed")
            curr_copy.languages = "\(languagesField.text!)".split(separator: ",").map { String($0)}
        }, for: .editingChanged)

        roleSeg.addAction(UIAction { _ in
            print("role changed")
            if (roleSeg.selectedSegmentIndex == 0) {
                curr_copy.role = .Professor
            } else if (roleSeg.selectedSegmentIndex == 1){
                curr_copy.role = .TA
            } else {
                curr_copy.role = .Student
            }
        }, for: .valueChanged)
        
        clearButton.addAction(UIAction { _ in
            print("clear")
            searchField.text = ""
            self.renderBlank()
            descriptionLabel.text = "Add new one's Info and ues Add button below to upload"
//            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
        }, for: .touchUpInside)
        
        searchButton.addAction(UIAction { _ in
            print("search")
            var searched : [Int] = []
            if (searchField.text == "") {
                for i in 0..<total{
                    searched.append(i)
                }
            } else {
                for idx in members.indices {
                    if (members[idx].netID == "\(searchField.text!)") {
                        searched.append(idx)
                    }
                }
            }
            if (searched.count > 0) {
                searched_idx = searched
                page_num = 0
                self.renderUpdate()
                descriptionLabel.text = "search successfully (3s)"
                Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
            } else {
                searchField.text = ""
                self.renderBlank()
                descriptionLabel.text = "search failed, no one matched (3s)"
                Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.fireFail), userInfo: nil ,repeats: false)
            }

        }, for: .touchUpInside)
        
        addButton.addAction(UIAction { _ in
            print("add")
            if (!blankMode) {
//                descriptionLabel.text = "You need to add profile from blank page, try clear button or you could build it when searching failed"
//                Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
                return
            }
            members.append(curr_copy)
            page_num = 0
            self.renderUpdate()
            descriptionLabel.text = "add successfully (3s)"
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
        }, for: .touchUpInside)
        
        updateButton.addAction(UIAction { _ in
            print("update")
            if (blankMode) {
//                descriptionLabel.text = "You need to browse/search the existing profile to update"
//                Timer.scheduledTimer(timeInterval: 6, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
                return
            }
            members[searched_idx[page_num]] = curr_copy
            descriptionLabel.text = "update successfully (3s)"
            Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(self.fire), userInfo: nil ,repeats: false)
        }, for: .touchUpInside)

        nextButton.addAction(UIAction { _ in
            print("next page")
            if (page_num != searched_idx.count - 1){
                page_num += 1
                self.renderUpdate()
            }
        }, for: .touchUpInside)
        
        lastButton.addAction(UIAction { _ in
            print("last page")
            if (page_num != 0){
                page_num -= 1
                self.renderUpdate()
            }
        }, for: .touchUpInside)
    }
    
}
// Don't change the following line - it is what allows the view controller to show in the Live View window
PlaygroundPage.current.liveView = HW1ViewController()
