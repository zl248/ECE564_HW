//
//  DukePersonTableViewCell.swift
//  ECE564_HW
//
//  Created by 李舳 on 2021/10/5.
//  Copyright © 2021 ECE564. All rights reserved.
//

import UIKit

class DukePersonTableViewCell: UITableViewCell {
    
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var desp: UITextView!
    
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
//    }

}
