//
//  ListTableViewController.swift
//  ECE564_HW
//
//  Created by 李舳 on 2021/10/5.
//  Copyright © 2021 ECE564. All rights reserved.
//

import UIKit

let simpleNotePadFilename = "data.json"
var documentURL: URL?
//func initData() -> Store {
//    var store = Store()
//    documentURL = getDocumentsDirectory().appendingPathComponent(simpleNotePadFilename)
//    let filemgr = FileManager.default
//    let encoder = JSONEncoder()
//    let decoder = JSONDecoder()
//    encoder.outputFormatting = .prettyPrinted
    
//    if filemgr.fileExists(atPath: documentURL!.path) {
//        print("existed")
//        exist_sign = true
//        do {
//            let data = (try? Data(contentsOf: documentURL!))!
//            print()
//            let dukeMembers = try? decoder.decode(Store.self, from: data)
//            store.loadMember(newMembers: dukeMembers!.members)
////            try filemgr.removeItem(at: documentURL!)
//        }
//        catch {
//
//        }
//    } else {
//        print("not existed")
//        exist_sign = false
//        store = store
//            .addMember(newMember: DukePerson(netid: "zl248", firstname: "Zhu", lastname: "Li", wherefrom: "China", gender: Gender.Male, hobbies: ["reading", "tennis", "hiking"], degree: "MENG", languages: ["Javasript", "C++", "Java"], team: "3", role: .Student))
//        do  {
//            let main = store.getMembers()[0];
//            main.picture = stringFromImage(UIImage(named: "zhu.jpeg")!);
//            store.updateMember(index: 0, updatedMember: main)
//            let jsondata = try encoder.encode(store)
//            print(jsondata);
//            try jsondata.write(to: documentURL!)
//            let data = (try? Data(contentsOf: documentURL!))!
//            let dukeMembers = try? decoder.decode(Store.self, from: data)
//            store.loadMember(newMembers: dukeMembers!.members)
//        } catch {
//            print("fail")
//        }
//    }
//    _store = downloadData(this: self)
//    saveData(members: _store.getMembers())
//    return store
//}

//let _store = initData()
var _store = Store()
var curr_members: [DukePerson] = []
var _sections:[dukeSection] = []

var mode = ""
var editRow = 0
class ListTableViewController: UITableViewController {
    var searched_idx: [Int] = []
    
    @IBOutlet weak var newPerson: UIBarButtonItem!
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as! UIBarButtonItem) == self.newPerson) {
            mode = "add"
            print("in")
        }
        print("mode ............")
        // fill the new duke person info to transfer to listview
        
        
        // also, upload!
    }
    let downloadImg = UIImage(named: "download.png")
    let downloadButton = UIButton()
    let searchButton = UIButton()
    let searchImg = UIImage(named: "find.png")
    let netidTextField = UITextField()
    // dukemembers
    override func viewDidLoad() {
        documentURL = getDocumentsDirectory().appendingPathComponent(simpleNotePadFilename)
        super.viewDidLoad()
        _store = downloadData(this: self)
        
        // test
//        let filemgr = FileManager.default
//        let encoder = JSONEncoder()
//        let decoder = JSONDecoder()
//        encoder.outputFormatting = .prettyPrinted
//        if filemgr.fileExists(atPath: documentURL!.path) {
//            print("existed")
//            exist_sign = true
//            do {
//                let data = (try? Data(contentsOf: documentURL!))!
//                print()
//                let dukeMembers = try? decoder.decode(Store.self, from: data)
//                print(dukeMembers!.members)
//                _store.loadMember(newMembers: dukeMembers!.members)
//    //            try filemgr.removeItem(at: documentURL!)
//            }
//            catch {
//
//            }
//        }
        
        saveData(members: _store.getMembers())
//        self._store = initData()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        downloadButton.frame = CGRect(x: 215, y: 23, width: 50, height: 32)
        downloadButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
        downloadButton.setImage(downloadImg, for: UIControl.State())
        downloadButton.layer.cornerRadius = 6
        downloadButton.isHidden = false
        downloadButton.titleLabel?.numberOfLines = 0
        downloadButton.titleLabel?.textAlignment = .center
        downloadButton.titleLabel?.lineBreakMode = .byWordWrapping

        searchButton.frame = CGRect(x: 295, y: 23, width: 50, height: 32)
        searchButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
        searchButton.setImage(searchImg, for: UIControl.State())
        searchButton.layer.cornerRadius = 6
        searchButton.isHidden = false
        searchButton.titleLabel?.numberOfLines = 0
        searchButton.titleLabel?.textAlignment = .center
        searchButton.titleLabel?.lineBreakMode = .byWordWrapping

        netidTextField.frame = CGRect(x: 30, y: 23, width: 150, height: 32)
        netidTextField.borderStyle = UITextField.BorderStyle.roundedRect
        netidTextField.autocapitalizationType = .none
        netidTextField.textColor = .black
        netidTextField.placeholder = "e.g. zl248"
        netidTextField.font = UIFont(name: "Cochin", size: 17)
        netidTextField.inputView = UIView()
        
        searchButton.addAction(UIAction { [self] _ in
            print("search")
            if (self.netidTextField.text == "") {
                curr_members = _store.getMembers()
            } else {
                curr_members = []
                for idx in _store.getMembers().indices {
                    if (self.netidTextField.text == "*") {
                        curr_members.append(_store.getMembers()[idx])
                    } else if ("\(self.netidTextField.text!)".first == "*") {
                        let str = "a\(self.netidTextField.text!)"
                        let sample = String(str.split(separator: "*")[1])
                        if (_store.getMembers()[idx].netid.hasSuffix(sample)) {
                            curr_members.append(_store.getMembers()[idx])
                        }
                    } else if ("\(self.netidTextField.text!)".last == "*") {
                        let str = "\(self.netidTextField.text!)a"
                        let sample = String(str.split(separator: "*")[0])
                        if (_store.getMembers()[idx].netid.hasPrefix(sample)) {
                            curr_members.append(_store.getMembers()[idx])
                        }
                    } else if ("\(self.netidTextField.text!)".split(separator: "*").count == 2 && _store.getMembers()[idx].netid.hasPrefix("\(self.netidTextField.text!)".split(separator: "*")[0]) && _store.getMembers()[idx].netid.hasSuffix("\(self.netidTextField.text!)".split(separator: "*")[1])) {
                        curr_members.append(_store.getMembers()[idx])
                    }
                }
            }
            _sections = getDukeSection()
            tableView.reloadData()
        }, for: .touchUpInside)
        
        downloadButton.addAction(UIAction { [self] _ in
            print("download")
            _store = downloadData(this: self)
            print(_store.getMembers())
            saveData(members: _store.getMembers())
            self.tableView.reloadData()
            
        }, for: .touchUpInside)
        
        view.addSubview(searchButton)
        view.addSubview(downloadButton)
        view.addSubview(netidTextField)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return _sections.count
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
//        for s in _sections {
//            print(s.sectionName)
//            for m in s.members {
//                print(m.firstname)
//            }
//
//        }
        return _sections[section].members.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return _sections[section].sectionName
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DukePersonCell", for: indexPath) as! DukePersonTableViewCell
//        if (indexPath.row >= _store.getMembers().count) {
//            return cell
//        }
        let tempDukePerson:DukePerson = _sections[indexPath.section].members[indexPath.row]
        cell.desp.text = tempDukePerson.description
        cell.desp.isEditable = false
        cell.label.text = tempDukePerson.firstname + tempDukePerson.lastname
        cell.iconImageView.image = imageFromString(tempDukePerson.picture)

        return cell
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView(frame: CGRect(x:0, y:0, width: tableView.frame.width, height: 35))
        view.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.586, green: 0.87, blue: 0.95, alpha: 1))
        let lbl = UILabel(frame: CGRect(x:50, y:0, width: view.frame.width-15, height: 35))
        lbl.text = _sections[section].sectionName
        let image = UIImageView(image: UIImage(named: "team.png"))
        image.frame = CGRect(x: 10, y: 0, width: 35, height: 35)
        lbl.textColor = .white
        view.addSubview(lbl)
        view.addSubview(image)
        
        return view
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(identifier: "info") as? InformationViewController
//        print(_store.getMembers()[indexPath.row].description)
        mode = "update"
        editRow = indexPath.row
        vc?.original = curr_members[indexPath.row]
        self.navigationController?.pushViewController(vc!, animated: true)
    }
    
    // see delegation for details.
//    override func tableView
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func returnFromNewPerson(segue: UIStoryboardSegue) {
        let source:InformationViewController = segue.source as! InformationViewController
//        let item:DukePerson = source.dukeperson
//        self.dukeMembers.append(item);
        self.tableView.reloadData()
    }
}
