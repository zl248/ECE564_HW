//
//  InformationViewController.swift
//  ECE564_HW
//
//  Created by 李舳 on 2021/9/5.
//  Copyright © 2021 ECE564. All rights reserved.
//

import UIKit

//var documentURL: URL? 
//let simpleNotePadFilename = "data.json"
var exist_sign: Bool = false
func saveData(members: [DukePerson]) -> Void {
    let encoder = JSONEncoder()
    do  {
        let store = Store()
        store.loadMember(newMembers: members);
        let jsondata = try encoder.encode(store)
        try jsondata.write(to: documentURL!)
    } catch {
        print("fail")
    }
}

class InformationViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var saveButton: UIBarButtonItem!
    var original : DukePerson = DukePerson()
    var curr_copy : DukePerson = DukePerson()
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if ((sender as! UIBarButtonItem) == self.saveButton) {
            print(mode)
            if (mode == "add") {
                print("add")
//                if (validate() == false) {return;}
                _store.addMember(newMember: self.curr_copy)
                curr_members = _store.getMembers()
                _sections = getDukeSection()
                saveData(members: _store.getMembers())
                uploadData(person: self.curr_copy)
                mode = ""
            } else if (mode == "update") {
                uploadData(person: self.curr_copy)
                _store.updateMember(index: editRow, updatedMember: self.curr_copy)
                curr_members = _store.getMembers()
                _sections = getDukeSection()
                saveData(members: _store.getMembers())
            }
            return;
        }
        // fill the new duke person info to transfer to listview
        print("mode")
        print(mode);
        
        // also, upload!
    }
    
    var imagePicker: UIImagePickerController = UIImagePickerController()
    lazy var imageButton = UIButton()
    let netidLabel = UILabel()
    let netidTextField = UITextField()
    let firstnameTextField = UITextField()
    let lastnameTextField = UITextField()
    let fromLabel = UILabel()
    let fromTextField = UITextField()
    let genderLabel = UILabel()
    let genderSeg = UISegmentedControl.init()
    let degreeLabel = UILabel()
    let degreeField = UITextField()
    let hobbiesLabel = UILabel()
    let hobbiesField = UITextField()
    let languagesLabel = UILabel()
    let languagesField = UITextField()
    let roleLabel = UILabel()
    let roleSeg = UISegmentedControl.init()
    let teamLabel = UILabel()
    let teamField = UITextField()
    let emailLabel = UILabel()
    let emailField = UITextField()
    let descriptionLabel = UILabel()
    let imageView = UIImageView(image: UIImage(named: "duke.jpg"))
    let addButton = UIButton()
    let updateButton = UIButton()
    
    var page_num = 0
    var members : [DukePerson] = []
    var total = 0
    var curr_total = 0
    var searched_idx: [Int] = []

    let blankTemplate = DukePerson()
    var blankMode = false
    
    @objc func fire() {
        descriptionLabel.text = curr_copy.description
    }
    
    @objc func savedImage(_ im:UIImage, error:Error?, context:UnsafeMutableRawPointer?) {
        if let err = error {
            print(err)
            return
        }
        print("success")
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    @objc func pickAnImage(){
        present(imagePicker, animated: true) {
            print("UIImagePickerController: presented")
        }
    }
        
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        guard let selectedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
            fatalError("error: did not picked a photo")
        }
        picker.dismiss(animated: true) { [unowned self] in
            // add a image view on self.view
            self.imageButton.setBackgroundImage(selectedImage, for: .normal)
            curr_copy.picture = stringFromImage(selectedImage)
        }
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) {
            print("UIImagePickerController: dismissed")
        }
    }
    
    func renderUpdate() {
        print("render update")
        blankMode = false
        total = members.count
        print(members)
        print(total)
//            print(curr_copy.netid)
//            print(curr_copy.picture)
        if (curr_copy.picture != ""){
            imageButton.setBackgroundImage(imageFromString(curr_copy.picture), for: .normal)
        }
        curr_total = searched_idx.count
        netidTextField.text = curr_copy.netid
        firstnameTextField.text = curr_copy.firstname
        lastnameTextField.text = curr_copy.lastname
        teamField.text = curr_copy.team
        fromTextField.text = curr_copy.wherefrom
        genderSeg.selectedSegmentIndex = curr_copy.gender == .Male ? 0: curr_copy.gender == .Female ? 1 : 2
        degreeField.text = curr_copy.degree
        hobbiesField.text = curr_copy.hobbies.joined(separator: ",")
        languagesField.text = curr_copy.languages.joined(separator: ",")
        roleSeg.selectedSegmentIndex = curr_copy.role == .Professor ? 0 : curr_copy.role == .TA ? 1 : 2
        descriptionLabel.text = curr_copy.description
        emailField.text = curr_copy.email
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
        fromTextField.isEnabled = false
        netidTextField.isEnabled = false
        firstnameTextField.isEnabled = false
        lastnameTextField.isEnabled = false
        teamField.isEnabled = false
        genderSeg.isEnabled = false
        degreeField.isEnabled = false
        hobbiesField.isEnabled = false
        languagesField.isEnabled = false
        roleSeg.isEnabled = false
        imageButton.isEnabled = false
        print("information load")
        print(curr_copy.description);
        curr_copy = original.getCopy()
        renderUpdate()
//        members = _store.getMembers()
        total = members.count
//        curr_copy = blankTemplate;
        
        setProperties(netidLabel: netidLabel, netidTextField: netidTextField,
                      firstnameTextField: firstnameTextField,
                      lastnameTextField: lastnameTextField, fromLabel: fromLabel,
                      fromTextField: fromTextField, genderLabel: genderLabel,
                      genderSeg: genderSeg, degreeLabel: degreeLabel,
                      degreeField: degreeField, hobbiesLabel: hobbiesLabel,
                      hobbiesField: hobbiesField, languagesLabel: languagesLabel,
                      languagesField: languagesField, roleLabel: roleLabel,
                      roleSeg: roleSeg, teamLabel: teamLabel,
                      teamField: teamField,
                      emailLabel: emailLabel, emailField: emailField,
                      descriptionLabel: descriptionLabel, imageView: imageView,
                      imageButton: imageButton,
                      updateButton: updateButton,
                      view: view);
        loadAction()
//        renderUpdate()
        
//        func fire() {
//            descriptionLabel.text = curr_copy.description
//        }
        
        func renderUpdate() {
            print("render update")
            blankMode = false
            total = members.count
            if (curr_copy.picture != ""){
                imageButton.setBackgroundImage(imageFromString(curr_copy.picture), for: .normal)
            }
            curr_total = searched_idx.count
            netidTextField.text = curr_copy.netid
            firstnameTextField.text = curr_copy.firstname
            lastnameTextField.text = curr_copy.lastname
            teamField.text = curr_copy.team
            fromTextField.text = curr_copy.wherefrom
            genderSeg.selectedSegmentIndex = curr_copy.gender == .Male ? 0: curr_copy.gender == .Female ? 1 : 2
            degreeField.text = curr_copy.degree
            hobbiesField.text = curr_copy.hobbies.joined(separator: ",")
            languagesField.text = curr_copy.languages.joined(separator: ",")
            roleSeg.selectedSegmentIndex = curr_copy.role == .Professor ? 0 : curr_copy.role == .TA ? 1 : 2
            descriptionLabel.text = curr_copy.description
            emailField.text = curr_copy.email
        }
        
        func loadAction() {// UI event
            updateButton.addAction(UIAction { _ in
                self.fromTextField.isEnabled = true
                self.netidTextField.isEnabled = true
                self.firstnameTextField.isEnabled = true
                self.lastnameTextField.isEnabled = true
                self.teamField.isEnabled = true
                self.genderSeg.isEnabled = true
                self.degreeField.isEnabled = true
                self.hobbiesField.isEnabled = true
                self.languagesField.isEnabled = true
                self.roleSeg.isEnabled = true
                self.imageButton.isEnabled = true
                if (self.updateButton.currentTitle == "Save") {
                    uploadData(person: self.curr_copy)
                    _store.updateMember(index: editRow, updatedMember: self.curr_copy)
                    _sections = getDukeSection()
                    saveData(members: _store.getMembers())
                }
                self.updateButton.setTitle("Save", for: UIControl.State())
                
            }, for: .allTouchEvents)
            netidTextField.addAction(UIAction { _ in
                print("net id changed")
                self.curr_copy.netid = self.netidTextField.text ?? ""
            }, for: .editingChanged)
            
            firstnameTextField.addAction(UIAction { _ in
                print("name changed")
                self.curr_copy.firstname = self.firstnameTextField.text ?? ""
            }, for: .editingChanged)

            lastnameTextField.addAction(UIAction { _ in
                print("name changed")
                self.curr_copy.lastname = self.lastnameTextField.text ?? ""
            }, for: .editingChanged)

            fromTextField.addAction(UIAction { _ in
                print("from changed")
                self.curr_copy.wherefrom = self.fromTextField.text ?? ""
            }, for: .editingChanged)

            genderSeg.addAction(UIAction { _ in
                print("gender changed")
                if (self.genderSeg.selectedSegmentIndex == 0) {
                    self.curr_copy.gender = .Male
                } else if (self.genderSeg.selectedSegmentIndex == 1){
                    self.curr_copy.gender = .Female
                } else {
                    self.curr_copy.gender = .NonBinary
                }
            }, for: .valueChanged)
            
            degreeField.addAction(UIAction { _ in
                print("degree changed")
                self.curr_copy.degree = self.degreeField.text ?? ""
            }, for: .editingChanged)

            hobbiesField.addAction(UIAction { _ in
                print("hobbies changed")
                self.curr_copy.hobbies = "\(self.hobbiesField.text!)".split(separator: ",").map { String($0)}
            }, for: .editingChanged)

            languagesField.addAction(UIAction { _ in
                print("languages changed")
                self.curr_copy.languages = "\(self.languagesField.text!)".split(separator: ",").map { String($0)}
            }, for: .editingChanged)

            roleSeg.addAction(UIAction { _ in
                print("role changed")
                if (self.roleSeg.selectedSegmentIndex == 0) {
                    self.curr_copy.role = .Professor
                } else if (self.roleSeg.selectedSegmentIndex == 1){
                    self.curr_copy.role = .TA
                } else {
                    self.curr_copy.role = .Student
                }
            }, for: .valueChanged)
            
            teamField.addAction(UIAction { _ in
                print("team changed")
                self.curr_copy.team = self.teamField.text ?? ""
            }, for: .allEditingEvents)
            
            imageButton.addAction(UIAction { _ in
                if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
                    self.imagePicker.delegate = self
                    self.imagePicker.sourceType = .photoLibrary
                    self.imagePicker.allowsEditing = false
                    self.present(self.imagePicker, animated: true)
                }
                print("image Button")
            }, for: .touchUpInside)
        }
        
        
        func validate() -> Bool {
            // step 0 check empty
            if (netidTextField.text?.count == 0 || firstnameTextField.text?.count == 0 ||
                    lastnameTextField.text?.count == 0 || fromTextField.text?.count == 0 ||
                    hobbiesField.text?.count == 0 || languagesField.text?.count == 0
            ) {
                descriptionLabel.text = "Invalid info: there should be no empty field"
                return false;
            }
            
            // step 1 check netid
            if (netidTextField.text?.count != 5) {
                descriptionLabel.text = "Invalid info: netid should be 5 letters & digits in total"
                return false;
            }
            
            // step 2 check name
            if (firstnameTextField.text?.count ?? 0 >= 15) {
                descriptionLabel.text = "Invalid info: fist & last name should be < 15 letters"
                return false;
            }
            if (lastnameTextField.text?.count ?? 0 >= 15) {
                descriptionLabel.text = "Invalid info: fist & last name should be < 15 letters"
                return false;
            }
            
            // step 3 check address
            if (firstnameTextField.text?.count ?? 0 >= 20) {
                descriptionLabel.text = "Invalid info: fist & last name should be < 20 letters"
                return false;
            }
            
            // step 4 check hobbies and langages
            if (curr_copy.hobbies.count > 3) {
                descriptionLabel.text = "Invalid info: hobbies can not exceed 3"
                return false;
            }
            
            if (curr_copy.languages.count > 3) {
                descriptionLabel.text = "Invalid info: languages can not exceed 3"
                return false;
            }
            
            return true;
        }
        func renderBlank() {
            print("render blank")
//            avatar.backgroundColor = .none
            addButton.isEnabled = true
            addButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
            imageButton.setBackgroundImage(UIImage(contentsOfFile: curr_copy.picture), for: .normal)
            updateButton.backgroundColor = .lightGray
            updateButton.isEnabled = false
            blankMode = true
            curr_copy = blankTemplate.getCopy();
            page_num = 0
            curr_total = 1
            netidTextField.text = curr_copy.netid
            firstnameTextField.text = curr_copy.firstname
            lastnameTextField.text = curr_copy.lastname
            fromTextField.text = curr_copy.wherefrom
            genderSeg.selectedSegmentIndex = curr_copy.gender == .Male ? 0 : curr_copy.gender == .Female ? 1: 2
            degreeField.text = curr_copy.degree
            hobbiesField.text = curr_copy.hobbies.joined(separator: ",")
            languagesField.text = curr_copy.languages.joined(separator: ",")
            roleSeg.selectedSegmentIndex = curr_copy.role == .Professor ? 0 : curr_copy.role == .TA ? 1 : 2
            teamField.text = curr_copy.team
            emailField.text = curr_copy.email
            descriptionLabel.text = curr_copy.description
        }
    }
    
}
