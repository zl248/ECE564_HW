//
//  UIConfig.swift
//  ECE564_HW
//
//  Created by 李舳 on 2021/9/24.
//  Copyright © 2021 ECE564. All rights reserved.
//
import UIKit
import Foundation

func setProperties(netidLabel: UILabel,
                   netidTextField: UITextField, firstnameTextField: UITextField,
                   lastnameTextField: UITextField, fromLabel: UILabel,
                   fromTextField: UITextField, genderLabel: UILabel,
                   genderSeg: UISegmentedControl, degreeLabel: UILabel,
                   degreeField: UITextField, hobbiesLabel: UILabel,
                   hobbiesField: UITextField, languagesLabel: UILabel,
                   languagesField: UITextField, roleLabel: UILabel,
                   roleSeg: UISegmentedControl, teamLabel: UILabel,
                   teamField: UITextField,
                   emailLabel: UILabel, emailField: UITextField,
                   descriptionLabel: UILabel, imageView: UIImageView,
                   imageButton: UIButton,
                   updateButton: UIButton,
                   view: UIView) {
    
//    downloadButton.frame = CGRect(x: 175, y: 73, width: 50, height: 32)
//    downloadButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
//    downloadButton.setImage(downloadImg, for: UIControl.State())
//    downloadButton.layer.cornerRadius = 6
//    downloadButton.isHidden = false
//    downloadButton.titleLabel?.numberOfLines = 0
//    downloadButton.titleLabel?.textAlignment = .center
//    downloadButton.titleLabel?.lineBreakMode = .byWordWrapping
//
//    searchButton.frame = CGRect(x: 295, y: 73, width: 50, height: 32)
//    searchButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
//    searchButton.setImage(searchImg, for: UIControl.State())
//    searchButton.layer.cornerRadius = 6
//    searchButton.isHidden = false
//    searchButton.titleLabel?.numberOfLines = 0
//    searchButton.titleLabel?.textAlignment = .center
//    searchButton.titleLabel?.lineBreakMode = .byWordWrapping
//
    netidLabel.frame = CGRect(x: 50, y: 77, width: 100, height: 20)
    netidLabel.text = "Net ID:"
    netidLabel.textColor = .black
    netidLabel.font = UIFont(name: "Noteworthy", size: 18)
    
    netidTextField.frame = CGRect(x: 160, y: 73, width: 185, height: 30)
    netidTextField.borderStyle = UITextField.BorderStyle.roundedRect
    netidTextField.autocapitalizationType = .none
    netidTextField.textColor = .black
    netidTextField.placeholder = "e.g. zl248"
    netidTextField.font = UIFont(name: "Cochin", size: 17)
    netidTextField.inputView = UIView()

    firstnameTextField.frame = CGRect(x: 50, y: 115, width: 140, height: 30)
    firstnameTextField.borderStyle = UITextField.BorderStyle.roundedRect
    firstnameTextField.textColor = .black
    firstnameTextField.placeholder = "e.g. Zhu"
    firstnameTextField.font = UIFont(name: "Cochin", size: 17)
    firstnameTextField.inputView = UIView()

    lastnameTextField.frame = CGRect(x: 205, y: 115, width: 140, height: 30)
    lastnameTextField.borderStyle = UITextField.BorderStyle.roundedRect
    lastnameTextField.textColor = .black
    lastnameTextField.placeholder = "e.g. Li"
    lastnameTextField.font = UIFont(name: "Cochin", size: 17)
    lastnameTextField.inputView = UIView()

    fromLabel.frame = CGRect(x: 50, y: 160, width: 100, height: 20)
    fromLabel.text = "From:"
    fromLabel.textColor = .black
    fromLabel.font = UIFont(name: "Noteworthy", size: 18)

    fromTextField.frame = CGRect(x: 160, y: 155, width: 185, height: 30)
    fromTextField.borderStyle = UITextField.BorderStyle.roundedRect
    fromTextField.textColor = .black
    fromTextField.placeholder = "e.g. China"
    fromTextField.font = UIFont(name: "Cochin", size: 17)
    fromTextField.inputView = UIView()

    genderLabel.frame = CGRect(x: 50, y: 200, width: 100, height: 20)
    genderLabel.text = "Gender:"
    genderLabel.textColor = .black
    genderLabel.font = UIFont(name: "Noteworthy", size: 18)

    genderSeg.insertSegment(withTitle: "Male", at: 0, animated: false)
    genderSeg.insertSegment(withTitle: "Female", at: 1, animated: false)
    genderSeg.insertSegment(withTitle: "Non-Bi", at: 2, animated: false)
    genderSeg.frame = CGRect(x: 160, y: 195, width: 185, height: 30)
    genderSeg.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.5))
    genderSeg.tintColor = .black

    degreeLabel.frame = CGRect(x: 50, y: 240, width: 100, height: 20)
    degreeLabel.text = "Degree:"
    degreeLabel.textColor = .black
    degreeLabel.font = UIFont(name: "Noteworthy", size: 18)
    
    degreeField.frame = CGRect(x: 160, y: 235, width: 185, height: 30)
    degreeField.borderStyle = UITextField.BorderStyle.roundedRect
    degreeField.textColor = .black
    degreeField.placeholder = ""
    degreeField.font = UIFont(name: "Cochin", size: 17)
    degreeField.inputView = UIView()
    
    hobbiesLabel.frame = CGRect(x: 50, y: 280, width: 100, height: 20)
    hobbiesLabel.text = "Hobbies:"
    hobbiesLabel.textColor = .black
    hobbiesLabel.font = UIFont(name: "Noteworthy", size: 18)

    hobbiesField.frame = CGRect(x: 160, y: 275, width: 185, height: 30)
    hobbiesField.borderStyle = UITextField.BorderStyle.roundedRect
    hobbiesField.textColor = .black
    hobbiesField.placeholder = "e.g. Hiking, Tennis"
    hobbiesField.font = UIFont(name: "Cochin", size: 17)
    hobbiesField.inputView = UIView()

    languagesLabel.frame = CGRect(x: 50, y: 320, width: 100, height: 20)
    languagesLabel.text = "Languages:"
    languagesLabel.textColor = .black
    languagesLabel.font = UIFont(name: "Noteworthy", size: 18)

    languagesField.frame = CGRect(x: 160, y: 315, width: 185, height: 30)
    languagesField.borderStyle = UITextField.BorderStyle.roundedRect
    languagesField.textColor = .black
    languagesField.placeholder = "e.g. Javascript, C++"
    languagesField.font = UIFont(name: "Cochin", size: 17)
    languagesField.inputView = UIView()

    roleLabel.frame = CGRect(x: 50, y: 360, width: 100, height: 20)
    roleLabel.text = "Role:"
    roleLabel.textColor = .black
    roleLabel.font = UIFont(name: "Noteworthy", size: 18)

    roleSeg.insertSegment(withTitle: "Prof", at: 0, animated: false)
    roleSeg.insertSegment(withTitle: "TA", at: 1, animated: false)
    roleSeg.insertSegment(withTitle: "Student", at: 2, animated: false)
    roleSeg.frame = CGRect(x: 160, y: 355, width: 185, height: 30)
    roleSeg.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.5))
    roleSeg.tintColor = .black
    
    teamLabel.frame = CGRect(x: 50, y: 400, width: 100, height: 20)
    teamLabel.text = "Team:"
    teamLabel.textColor = .black
    teamLabel.font = UIFont(name: "Noteworthy", size: 18)

    teamField.frame = CGRect(x: 160, y: 395, width: 185, height: 30)
    teamField.borderStyle = UITextField.BorderStyle.roundedRect
    teamField.textColor = .black
    teamField.placeholder = "e.g. Tapcoffee"
    teamField.font = UIFont(name: "Cochin", size: 17)
    teamField.inputView = UIView()

    
    emailLabel.frame = CGRect(x: 50, y: 440, width: 100, height: 20)
    emailLabel.text = "Email:"
    emailLabel.textColor = .black
    emailLabel.font = UIFont(name: "Noteworthy", size: 18)
    
    emailField.frame = CGRect(x: 160, y: 435, width: 185, height: 30)
    emailField.borderStyle = UITextField.BorderStyle.roundedRect
    emailField.textColor = .black
    emailField.placeholder = "(autofill)"
    emailField.font = UIFont(name: "Cochin", size: 17)
    emailField.isEnabled = false
    emailField.inputView = UIView()
    
    descriptionLabel.frame = CGRect(x: 50, y: 565, width: 295, height: 90)
    descriptionLabel.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.586, green: 0.87, blue: 0.95, alpha: 1))
    descriptionLabel.lineBreakMode = .byWordWrapping
    descriptionLabel.numberOfLines = 0
    descriptionLabel.textAlignment = .center
    descriptionLabel.textColor = .white
    descriptionLabel.adjustsFontSizeToFitWidth = true
    descriptionLabel.minimumScaleFactor = 0.1
    descriptionLabel.font = UIFont(name: "Noteworthy", size: 14)
    
    imageView.frame = CGRect(x: 65, y: 105, width: 250, height: 250)
    imageView.alpha = 0.06
    
    imageButton.frame = CGRect(x: 50, y: 480, width: 80, height: 80)
    imageButton.backgroundColor = .lightGray
    imageButton.layer.masksToBounds = true
    imageButton.layer.cornerRadius = 40
    imageButton.layer.borderWidth = 1
    imageButton.layer.borderColor = CGColor.init(gray: 0.7, alpha: 0.5)

    updateButton.frame = CGRect(x: 215, y: 500, width:75, height: 35)
    updateButton.backgroundColor = UIColor.init(cgColor: CGColor.init(red: 0.2, green: 0.85, blue: 1, alpha: 0.8))
    updateButton.layer.cornerRadius = 8
    updateButton.isHidden = false
    updateButton.titleLabel?.numberOfLines = 0
    updateButton.titleLabel?.textAlignment = .center
    updateButton.titleLabel?.lineBreakMode = .byWordWrapping
    updateButton.setTitle("Edit", for: UIControl.State())
    updateButton.titleLabel?.font = UIFont(name: "Noteworthy", size: CGFloat(18))
    updateButton.setTitleColor(UIColor.white, for: UIControl.State())
    updateButton.setTitleColor(UIColor.red, for: .highlighted)
    
    view.backgroundColor = .white
//    view.addSubview(searchButton)
//    view.addSubview(downloadButton)
    view.addSubview(netidLabel)
    view.addSubview(netidTextField)
    view.addSubview(firstnameTextField)
    view.addSubview(lastnameTextField)
    view.addSubview(fromLabel)
    view.addSubview(fromTextField)
    view.addSubview(genderLabel)
    view.addSubview(genderSeg)
    view.addSubview(degreeLabel)
    view.addSubview(degreeField)
    view.addSubview(hobbiesLabel)
    view.addSubview(hobbiesField)
    view.addSubview(languagesLabel)
    view.addSubview(languagesField)
    view.addSubview(roleLabel)
    view.addSubview(roleSeg)
    view.addSubview(teamLabel)
    view.addSubview(teamField)
    view.addSubview(emailLabel)
    view.addSubview(emailField)
    view.addSubview(descriptionLabel)
    view.addSubview(imageView)
    view.addSubview(imageButton)
    view.addSubview(updateButton)
}
