//
//  DataModel.swift
//  ECE564_HW
//
//  Created by 李舳 on 2021/9/6.
//  Copyright © 2021 ECE564. All rights reserved.
//
import UIKit
import Foundation

enum Gender : String, Codable {
    case Male = "Male"
    case Female = "Female"
    case NonBinary = "Non-binary"
    case Unknown = "unknown"
}

class Person : Codable{
    var firstname = ""
    var lastname = ""
    var wherefrom = ""  // this is just a free String - can be city, state, both, etc.
    var gender : Gender = .Male
    var hobbies : [String] = []
    
    private enum CodingKeys: String, CodingKey {
        case firstname
        case lastname
        case wherefrom
        case gender
        case hobbies
    }
    
    init() {}
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        firstname = try container.decode(String.self, forKey: .firstname)
        lastname = try container.decode(String.self, forKey: .lastname)
        wherefrom = try container.decode(String.self, forKey: .wherefrom)
        do {
            gender = try container.decode(Gender.self, forKey: .gender)
        } catch {
            gender = .Unknown
        }
        
        hobbies = try container.decode([String].self, forKey: .hobbies)
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(firstname, forKey: .firstname)
        try container.encode(lastname, forKey: .lastname)
        try container.encode(wherefrom, forKey: .wherefrom)
        try container.encode(gender, forKey: .gender)
        try container.encode(hobbies, forKey: .hobbies)
    }
}

enum DukeRole : String, Codable {
    case Student = "Student"
    case Professor = "Professor"
    case TA = "Teaching Assistant"
    case Other = "Other"
}

protocol ECE564 {
    var degree : String { get }
    var languages: [String] { get }
    var team : String { get }
}

// custom protocol
protocol CustomStringConvertible {
    var description: String { get }
}

// new sub class
class DukePerson : Person, ECE564, CustomStringConvertible {
    var netid : String = ""
    var id: String{
        get {
            return netid
        }
    }
    var degree : String = ""
    var languages : [String] = []
    var team : String = "1"
    var role : DukeRole = .Professor
    var department: String = "ECE"
    var picture: String = ""
    var email : String{
        get {
            return netid + "@duke.edu"
        }
    }
    
    private enum CodingKeys : String, CodingKey {
        case id
        case netid
        case degree
        case languages
        case team
        case role
        case department
        case picture
        case email
    }
    
    var description: String{
        get{
            return "This is \(firstname) \(lastname) from \(wherefrom)! \(self.gender == .Male ? "He" : "She") is a \(self.role == .Professor ? "professor" : self.role == .TA ? "TA" : "student") of team \(self.team) at Duke University, hobbies are \(self.hobbies.joined(separator: ",")), master \(self.languages.joined(separator: ","))"
        }
    }
    
    override init () {
        super.init()
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        try super.init(from: decoder)
        netid = try container.decode(String.self, forKey: .netid)
        degree = try container.decode(String.self, forKey: .degree)
        languages = try container.decode([String].self, forKey: .languages)
        team = try container.decode(String.self, forKey: .team)
        do {
            role = try container.decode(DukeRole.self, forKey: .role)
        } catch {
            role = .Other
        }
        department = try container.decode(String.self, forKey: .department)
        picture = try container.decode(String.self, forKey: .picture)
    }
    
    override func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try super.encode(to: encoder)
        try container.encode(id, forKey: .id)
        try container.encode(netid, forKey: .netid)
        try container.encode(degree, forKey: .degree)
        try container.encode(languages, forKey: .languages)
        try container.encode(team, forKey: .team)
        try container.encode(role, forKey: .role)
        try container.encode(email, forKey: .email)
        try container.encode(department, forKey: .department)
        try container.encode(picture, forKey: .picture)
    }
    
    init (netid: String, firstname: String, lastname: String, wherefrom: String, gender: Gender, hobbies: [String], degree: String, languages: [String], team: String, role: DukeRole) {
        super.init()
        self.netid = netid
        self.firstname = firstname
        self.lastname = lastname
        self.wherefrom = wherefrom
        self.gender = gender
        self.hobbies = hobbies
        self.degree = degree
        self.team = team
        self.languages = languages
        self.role = role
    }
    
    func getCopy() -> DukePerson {
        let newInstance = DukePerson()
        newInstance.netid = self.netid
        newInstance.firstname = self.firstname
        newInstance.lastname = self.lastname
        newInstance.wherefrom = self.wherefrom
        newInstance.gender = self.gender
        newInstance.degree = self.degree
        newInstance.hobbies = []
        for idx in self.hobbies.indices {
            newInstance.hobbies.append(self.hobbies[idx])
        }
        newInstance.degree = self.degree
        newInstance.team = self.team
        newInstance.role = self.role
        newInstance.languages = []
        for idx in self.languages.indices {
            newInstance.languages.append(self.languages[idx])
        }
        newInstance.picture = self.picture
        return newInstance
    }
}

class Store : Decodable, Encodable {
    var members : [DukePerson] = [];
    func loadMember(newMembers: [DukePerson]) -> Void {
        members = newMembers
    }
    func addMember(newMember: DukePerson) -> Store {
        members.append(newMember)
        return self
    }
    
    func updateMember(index: Int, updatedMember: DukePerson) -> Void {
        members[index] = updatedMember
    }
    
    func getMembers() -> [DukePerson] {
        return members;
    }
}

class dukeSection {
    var sectionName: String = "";
    var members: Array<DukePerson> = []
}

func getDukeSection() -> [dukeSection] {
    var dukeSections: [dukeSection] = []
    var list: [String] = []
    for person in curr_members {
        if list.contains(person.role.rawValue + ": " + person.team) {
            let i = list.firstIndex(of: person.role.rawValue + ": " + person.team)!
            dukeSections[i].members.append(person)
        } else {
            list.append(person.role.rawValue + ": " + person.team)
            let section = dukeSection()
            section.sectionName = person.role.rawValue + ": " + person.team
            section.members = [person]
            dukeSections.append(section)
        }
    }
    return dukeSections
}

func getDocumentsDirectory() -> URL {
    let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
    let documentsDirectory = paths[0]
    return documentsDirectory
}

func stringFromImage(_ imagePic: UIImage) -> String {
    let picImageData: Data = imagePic.jpegData(compressionQuality: 1)!
    let picBase64 = picImageData.base64EncodedString()
    return picBase64
}

func imageFromString(_ imageStr: String) -> UIImage {
    let imageData = Data.init(base64Encoded: imageStr, options: .init(rawValue: 0))
    let image = UIImage(data: imageData!)
//    print(image)
    return image!
}

func uploadData(person: DukePerson) -> Void {
    let Url = String(format: "http://kitura-fall-2021.vm.duke.edu:5640")
    guard let serviceUrl = URL(string: Url + "/b64entries") else { return }
    var request = URLRequest(url: serviceUrl)
    request.httpMethod = "POST"
    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json", forHTTPHeaderField: "Accept")
    guard let loginData = credentials.data(using: String.Encoding.utf8) else {
        return
    }
    let base64LoginStr = loginData.base64EncodedString()
    request.setValue("Basic \(base64LoginStr)", forHTTPHeaderField: "Authorization")
    let data = try? JSONEncoder().encode(person)
    print(data!)
    print(String(data: data!, encoding: .utf8)!)
    print(serviceUrl)
    request.httpBody = data
    request.timeoutInterval = 20
    let session = URLSession.shared
    session.dataTask(with: request) { (data, response, error) in
        if let response = response {
            print(response)
        }
        if let data = data {
            do {
                print(String(data: data, encoding: .utf8)!)
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                print(json)
            } catch {
                print(error)
            }
        }
    }.resume()
}

func downloadData(this: ListTableViewController ) -> Store {
    var store = Store()
    let Url = String(format: "http://kitura-fall-2021.vm.duke.edu:5640")
    guard let serviceUrl = URL(string: Url + "/b64entries") else { return store}
    var request = URLRequest(url: serviceUrl)
    request.httpMethod = "GET"
    request.setValue("Application/json", forHTTPHeaderField: "Content-Type")
    request.setValue("application/json", forHTTPHeaderField: "Accept")
    guard let loginData = credentials.data(using: String.Encoding.utf8) else {
        return store
    }
    let base64LoginStr = loginData.base64EncodedString()
    request.setValue("Basic \(base64LoginStr)", forHTTPHeaderField: "Authorization")
    print("Basic \(base64LoginStr)")
    request.timeoutInterval = 20
    let session = URLSession.shared
    session.dataTask(with: request) { (data, response, error) in
        if let response = response {
            print(response)
            print("response ------")
        }
        if let data = data {
            do {
//                print(String(data: data, encoding: .utf8)!)
                let decoder = JSONDecoder()
                let dukeMembers = try? decoder.decode([DukePerson].self, from: data)
                store = Store()
                store.loadMember(newMembers: dukeMembers!)
                print(store.getMembers())
                DispatchQueue.main.async {
                    _store = store
                    curr_members = store.getMembers()
                    _sections = getDukeSection()
                    this.tableView.reloadData()
                }
//                var searched : [Int] = []
//                for i in 0..<this.members.count{
//                    searched.append(i)
//                }
                saveData(members: store.getMembers())
            } catch {
                print(error)
            }
        }
    }.resume()
    return store;
}








let credentials = "zl248:B91CB5D0661220EB12D7C6C739583449"
