#   ECE564_HW 
This is the project you will use for all of your ECE564 homework assignments. You need to download to your computer, add your code, and then add a repo under your own ID with this name ("ECE564_HW"). It is important that you use the same project name.  
This should work fine for HW1 - HW4.  If you decide to use SwiftUI for your user interface, you can recreate with a new project (keep the name) or follow something like this - https://stackoverflow.com/questions/56529488/is-there-any-way-to-use-storyboard-and-swiftui-in-same-ios-xcode-project

Any notes, additional functions, comments you want to share with the TA and I before grading please put in this file in the correspondiing section below.  Part of the grading is anything you did above and beyond the requirements, so make sure that is included here in the README.

## HW1
* Well-organized code: data, UI config, render functions, with clean loadviewcontroller
* Neat UI design/expeience: well-selected fonts, layout, color, placeholder, Icon, background Image, linebreak
* Comlicated function: paging
* Nice swift experiment: computational properties (did it in description), timer (show 3s' notification)
* Well-designed data store: Store class to simulate the database in memory, getCopy method for data protection, addMembers method is chained - easy to call

## HW2
* Fancy UI component: pickerview at Team field
* Powerful validaton: check empty field, limit the max length of strings and don't allow hobbies and langauges exceeding 3
* Image support: I use netid.JPG to identify different people's avatar, now you can see mine and others are default gray bg 
* Powerful paging feature as you can see everyone's profile
* Email's field is special, I set it as computational property and it is disabled to edit.
* PS: empty search = search all in my app, you can quickly use clear + search to see all profiles

## HW3
* Strong UIImagePicker function: automatically load TA/students/professor's picture if not exsited
* Well-organized encoding and decofing function in DataModel
* Improved UI logic: "all" button to support paging
* Improved UI logic: "clear" button to support adding
* Set up Asyncronous task in descriptions


## HW4
* Download button
* Use * as any character(s) to match, *8 to match netid with suffix 8, 8\* to match prefix 8. * to match all.
* Now paging can support those advanced search.
* Organized code well, split funcitons out in UIConfig.swift

## HW5
* Customized header, style and class to seperate sections
* well-organized code structure
* Search has advanced feature


## HW6
add text here

## HW7
add text here
